from os import listdir
from os.path import isfile, join
htmlDataDir='./Data/HTML'
urlDataDir='./Data/URLS'


htmlFiles = [f for f in listdir(htmlDataDir) if isfile(join(htmlDataDir,f))]
urlDumpFiles = [urlDataDir + '/' + f + '.urlist' for f in htmlFiles]
htmlFiles = [htmlDataDir  + '/' + f for f in htmlFiles]
#print(htmlFiles)
#print(urlDumpFiles)

for i in range(len(htmlFiles)):
	htmlFile = htmlFiles[i]
	urlDumpFile = urlDumpFiles[i]
	x = ''
	urls = []
	for line in open(htmlFile,'r'):
		x += line.strip()

	while 'cell grid-item small-6 medium-4 js-fund-tile' in x:
		x = x[x.index('cell grid-item small-6 medium-4 js-fund-tile')+1:]
		x = x[x.index('<a href="')+len('<a href="'):]
		u = x.index('"')
		urls.append(x[:u])
		x = x[u+1:]

	#print(urls)
	with open(urlDumpFile,'w') as ow:
		for url in urls: ow.write(url+'\n')
