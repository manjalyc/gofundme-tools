import urllib.request
import urllib.parse
import html
import time
from datetime import datetime, timedelta
from os.path import exists as os_path_exists
from urllib.error import HTTPError
from os import listdir
from os.path import isfile, join
import sys

global genMasterCSV, masterCSV
genMasterCSV = True
masterCSV = './Data/CSVS/combined.csv'

global urlDataDir, cacheDataDir, outputCSVDataDir, baseURL
urlDataDir='./Data/URLS'
cacheDataDir='./Data/CACHE'
outputCSVDataDir='./Data/CSVS'
baseURL='https://www.gofundme.com/f/'

global cleaner, newlines
newlines = ['\u2028','\r','\v','\x0b','\f','\x0c','\x1c','\x1d','\x1e','\x85','\u2028','\u2029','*',' ;']
import re
cleaner = re.compile('<.*?>')
def sanitizeDescription(x):
	for i in newlines: x = x.replace(i,';')
	return re.sub(cleaner, '', x).replace('\xa0',' ').strip().replace(' \n ','; ').replace('\n',';').replace('  ',' ').replace('\t','  ').replace(';;',';')
def sanitizeForTSV(x):
	return sanitizeDescription(x)
	#return x.replace('\n',';').replace('\t', '  ')

#you need to cut it
def cutX(x, r):
	return x[ x.index(r) + len(r):]

#retrieve html from url and now offline caches it, should probably implement in other method but whatever
def inCache(cacheID):
	global cacheDataDir
	return os_path_exists(cacheDataDir + '/' + cacheID)

def getFromCache(cacheID):
	global cacheDataDir
	print(f'[CACHE][PULL]: {cacheID}')
	ret = ''
	for line in open(cacheDataDir + '/' + cacheID,'r'):
		ret += line
	return ret

def putIntoCache(cacheID, htmldata):
	global cacheDataDir
	print(f'[CACHE][INST]: {cacheID}')
	with open(cacheDataDir + '/' + cacheID,'w') as ow:
		ow.write(htmldata)
	return


def getHTML(url, user_agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"):
	cacheID = 'na'
	if url.index(baseURL) == 0:
		cacheID = url[27:]
		if inCache(cacheID): return getFromCache(cacheID)
	
	interval=1
	print(f"[WAIT]: Waiting {interval} seconds, before downloading {url}")
	time.sleep(interval)

	hdr = { 'User-Agent' : user_agent }
	req = urllib.request.Request(url, headers = hdr)
	ret = ''
	try:
		ret = html.unescape(urllib.request.urlopen(req).read().decode('UTF-8'))
	except:
		ret = 'Deactivated'
	putIntoCache(cacheID,ret)
	return ret


#get details from html string x
def getDetails(x):
	#print(x)
	y = x
	if "Deactivated" in x or 'currently disabled new donations' in x: return -1
	utx = x.find('a-created-date">Created ')
	if utx < 0: return -1
	cDi = utx+len('a-created-date">Created ')
	creationDate = x[cDi:x.index("</span>",cDi)]
	tt = creationDate.split(', ')
	if len(tt) == 2: creationDate = tt[1] + " " + tt[0]
	elif ' days ago' in x:
		creationDate = creationDate[:creationDate.index(' days ago')]
		backthen = datetime.now() - timedelta(days=int(creationDate))
		creationDate = backthen.strftime('%Y %B %d')
	elif ' hours ago' in x:
		creationDate = creationDate[:creationDate.index(' hours ago')]
		backthen = datetime.now() - timedelta(hours=int(creationDate))
		creationDate = backthen.strftime('%Y %B %d')
	else:
		creationDate = creationDate[:creationDate.index(' day ago')]
		backthen = datetime.now() - timedelta(days=int(creationDate))
		creationDate = backthen.strftime('%Y %B %d')
	
	
	
	
	
	author, title, recipent = '','',''
	#todo: work out edge case gunplatalk, fundraiser for is after fundraiser by
	if 'Fundraiser for ' in x:
		x = cutX(x, 'Fundraiser for ')
		recipent = x[:x.index(' by ')]
		x = cutX(x, ' by ')
	else:
		x = cutX(x, 'Fundraiser by ')
	
	try:
		title = x[:x.index("</title>")]
		tt = title.index(":")
		author = title[:tt]
		title = title[tt+1:]
		author = author.strip()
		title = title.strip()
	except ValueError:
		x = y
		title = x[:x.index("</title>")]
		tt = title.index(":")
		author = title[:tt]
		author = cutX(author, 'Fundraiser by ')
		recipent = recipent[:recipent.index("</title")]
		title = title[tt+1:]
		author = author.strip()
		title = title.strip()
	
	#x = cutX(x, '<span class="text-stat-value text-underline u-pointer">')
	#donors = x[:x.index("<")]
	#donors = '324'
	
	#x = cutX(x, '<li class="m-meta-list-item"><span class="text-stat"><span class="text-stat-value ">')
	#shares = x[:x.index("<")]
	#shares = '234'
	
	#x = cutX(x, '<li class="m-meta-list-item"><span class="text-stat"><span class="text-stat-value ">')
	#followers = x[:x.index("<")]
	#followers='231'
	x = cutX(x,'<div class="o-campaign-story">')
	description = x[:x.index("</div>")]
	
	
	x = cutX(x, '"currencycode":"')
	currType=x[:x.index('"')]
	x = cutX(x, '"current_amount":')
	raisedAmount=x[:x.index(',')]
	
	
	x = cutX(x, '"donation_count":')
	donors = x[:x.index(',')]
	#print(donors)
	x = cutX(x, '"goal_amount":')
	goal=x[:x.index(',')]
	
	x = cutX(x, '"campaign_hearts":')
	followers = x[:x.index(',')]
	
	x = cutX(x, '"social_share_total":')
	shares = x[:x.index(',')]

	numUpdates='0'
	uid=set()
	while '"update_id":' in y:
		y = cutX(y, '"update_id":')
		uid.add(y[:y.index(',')])
	numUpdates = str(len(uid))
	
	return [title, creationDate, numUpdates, raisedAmount, goal, donors, shares, followers, author, recipent, currType,  description]


def setDetails(data, url):
	l = ''
	try:
		l = getDetails(getHTML(url))
	except HTTPError as err:
		if err.code == 404:
			return
		else:
			raise
	if l == -1: return
	data[url] = dict()
	#print(url, l)
	if l == -1: return
	
	for i in range(len(l)): l[i] = l[i].replace(',',';')
	data[url] = dict()
	data[url]['title'] = sanitizeForTSV(l[0])
	data[url]['creationDate'] = sanitizeForTSV(l[1])
	data[url]['numUpdates'] = sanitizeForTSV(l[2])
	data[url]['raisedAmount'] = sanitizeForTSV(l[3])
	data[url]['goal'] = sanitizeForTSV(l[4])
	data[url]['donors'] = sanitizeForTSV(l[5])
	data[url]['shares'] = sanitizeForTSV(l[6])
	data[url]['followers'] = sanitizeForTSV(l[7])
	data[url]['author'] = sanitizeForTSV(l[8])
	data[url]['recipent'] = sanitizeForTSV(l[9])
	data[url]['currType'] = sanitizeForTSV(l[10])
	data[url]['description'] = sanitizeDescription(l[11])


#todo incremental updates for genCSV
def initCSV(data, outputCSV):
	o = open(outputCSV,'w')
	o.write('Title\tAuthor\tRecipent\tCreation Date\tMin* Updates\tCurrency\tAmount Raised\tGoal\tDonors\tShares\tFollowers\tURL\tDescription\n')
	o.close()
	

def appendCSV(data, url, outputCSV):
	o = open(outputCSV,'a')
	sep = "\t"
	o.write(data[url]['title'] + sep)
	o.write(data[url]['author'] + sep)
	o.write(data[url]['recipent'] + sep)
	o.write(data[url]['creationDate'] + sep)
	o.write(data[url]['numUpdates'] + sep)
	o.write(data[url]['currType'] + sep)
	o.write(data[url]['raisedAmount'] + sep)
	o.write(data[url]['goal'] + sep)
	o.write(data[url]['donors'] + sep)
	o.write(data[url]['shares'] + sep)
	o.write(data[url]['followers']+ sep)
	o.write(url + sep)
	o.write(data[url]['description'])
	if url == 'https://www.gofundme.com/f/GailC': print(data[url]['description'])
	o.write('\n')
	o.close()

def printKeyFromData(data,key):
	global genMasterCSV
	print(f"\tTitle: {data[key]['title']}, Author: {data[key]['author']}, Recipent: {data[key]['recipent']}, Creation Date: {data[key]['creationDate']},Updates: {data[key]['numUpdates']},Currency: {data[key]['currType']},Amount Raised: {data[key]['raisedAmount']},Goal: {data[key]['goal']},Donors: {data[key]['donors']},Shares: {data[key]['shares']},Followers: {data[key]['followers']}")
if __name__ == "__main__":
	if len(sys.argv) > 1: #troubleshoot first arg is cacheID
		data = dict()
		line = 'https://www.gofundme.com/f/' + sys.argv[-1]
		print(line)
		setDetails(data, line)
		print(data[line])
		sys.exit()

	urlInputFiles=[f for f in listdir(urlDataDir) if isfile(join(urlDataDir, f))]
	csvOutputFiles=[outputCSVDataDir + '/' + f + '.csv' for f in urlInputFiles]
	urlInputFiles=[urlDataDir + '/' + f for f in urlInputFiles]
	
	
	#todo unduplicate this  code
	if genMasterCSV:
		outputCSV = masterCSV
		urls = set()
		data=dict()
		initCSV(data,outputCSV)
		for i in range(len(urlInputFiles)):
			for line in open(urlInputFiles[i],'r'): urls.add(line.strip())
		for url in urls:
			setDetails(data, url)
			url = url.strip()
			if url in data:
				appendCSV(data, url, outputCSV)
				printKeyFromData(data,url)
			else:
				print("[DEACTIVATED]:",url)
				continue
		sys.exit()
	
	for i in range(len(urlInputFiles)):
		outputCSV = csvOutputFiles[i]
		inputURLFile = urlInputFiles[i]
		
		# mapped by url
		data=dict()
		initCSV(data,outputCSV)
		with open(inputURLFile) as u:
			for line in u:
				line = line.strip()
				if line[0] == "#": continue
				print(line)
				setDetails(data, line)
				if line in data:
					appendCSV(data, line, outputCSV)
					printKeyFromData(data,line)
				else:
					continue
					print("[DEACTIVATED]:",line)
