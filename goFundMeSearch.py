import time
from selenium import webdriver
#Use Chrome options, if chrome is installed allows visualization and easy debugging
from selenium.webdriver.chrome.options import Options

import sys
import urllib.parse
from datetime import datetime
from selenium.webdriver.common.action_chains import ActionChains

global driver, interval, outputDir, cclim, cctime
outputDir='./Data/HTML'
interval = 0
cclim=300
cctime=1

def startWebDriver():
	global driver
	options = Options()
	driver = webdriver.Chrome(chrome_options=options)

def initializeSearch(search_term):
	global driver, searchURL
	searchURL = 'https://www.gofundme.com/mvc.php?route=homepage_norma/search&term='+urllib.parse.quote(search_term)
	print(f"Search for '{search_term}' intialized; url = {searchURL}")
	driver.get(searchURL)

def clickShowMoreAndSave(outputHTMLFile):
	global driver, interval, searchURL, cclimit, cctime
	element = driver.find_element_by_xpath('/html/body/div[2]/div/div/div[2]/a')
	print(f"Output HTML file: {outputHTMLFile}")
	while True:
		print(f"\nClicking Show More..., Waiting {interval} secs...",end='')
		cc = 0
		while not element.is_displayed() or not element.is_enabled:
			print('.',end='')
			sys.stdout.flush()
			time.sleep(cctime)
			cc += 1
			if cc == 300:
				print(f'\nWaiting limit reached cc={cc}, cclim={cclim}, cctime={cctime}')
				break
		if driver.current_url != searchURL:
			break
		ActionChains(driver).move_to_element(element).click(element).perform()
		writePage(outputHTMLFile)
		time.sleep(interval)
	print()
	if driver.current_url != searchURL: print("ERROR: URL CHANGE to " + driver.current_url)

def writePage(outputHTMLFile):
	global driver
	with open(outputHTMLFile, 'w') as f:
		f.write(driver.page_source)

def searchAndStore(search_term):
	global interval, outputDir, driver
	outputHTMLFile=f"{outputDir}/output-{search_term}_{datetime.now().strftime('%Y-%m-%d@%H:%M:%S')}.html".replace(' ','-')
	startWebDriver()
	initializeSearch(search_term)
	clickShowMoreAndSave(outputHTMLFile)
	

if __name__ == "__main__":
	searchTerms=['alopecia','alopecia areata','alopecia wigs','alopecia areata wigs','hair loss wigs','hair loss','wigs']
	if sys.argv[-1] == '-1':
		print(len(searchTerms))
		print(searchTerms)
	elif sys.argv[-1].isnumeric(): searchAndStore(searchTerms[int(sys.argv[-1])])
	else: searchAndStore(input('Enter search term: '))
	
